package tn.esprit.spring.tests;

import java.text.ParseException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.repository.DepartementRepository;
import tn.esprit.spring.services.IEntrepriseService;
import tn.esprit.spring.utils.BaseJUnit49TestCase;
import tn.esprit.spring.repository.EntrepriseRepository;

public class EntrepriseTest extends BaseJUnit49TestCase{
	private static final Logger LOG = LogManager.getLogger(EntrepriseTest.class);

	@Autowired
	DepartementRepository deptRepoistory;
	@Autowired
	EntrepriseRepository entrepriserepo;
	@Autowired
	IEntrepriseService entrepriseservice;

	private Entreprise entreprise;
	private Departement departement;

	@Override
	public void setUp() throws Exception {
		super.setUp();
		this.departement = new Departement();
		this.departement.setName("Cour de soir");

		this.entreprise = new Entreprise();
		this.entreprise.setName("esprit");
		this.entreprise.setRaisonSocial("80000");
		entreprise.addDepartement(departement);
	}

	@Test
	public void tests() throws ParseException {
		ajouterEntrepriseTest();
		ajouterDepartementTest();
		affecterDepartementAEntrepriseTest();
		getEntrepriseByIdTest();
		deleteEntrepriseByIdTest();

		LOG.info("TEST OF ENTREPRISE FINISHED SUCCESSFULY. ");
	}



	public void ajouterEntrepriseTest() {
		LOG.info("--------------------- Start Method ADD Entreprise ------------------------");
		LOG.debug(this.entreprise);
		entrepriseservice.ajouterEntreprise(entreprise);
		Assert.assertTrue(entrepriseservice.ajouterEntreprise(entreprise) > 0);
		LOG.info("Departement has been Added successfly");
		LOG.info("--------------------- End Method ADD Entreprise --------------------------");
	}

	public void ajouterDepartementTest() {
		LOG.info("--------------------- Start Method ADD Departement1 ------------------------");
		LOG.debug(this.departement);
		entrepriseservice.ajouterDepartement(departement);
		Assert.assertTrue(entrepriseservice.ajouterDepartement(departement) > 0);
		LOG.info("Departement created  successfly");
		LOG.info("--------------------- out of Departement --------------------------");
	}

	public void affecterDepartementAEntrepriseTest() {
		LOG.info("------------------- Start Method affecter Departement An Entreprise ---------------------------");
		LOG.debug(this.departement);
		LOG.debug(this.entreprise);
		entrepriseservice.ajouterEntreprise(entreprise);
		LOG.info(departement.getEntreprise());
		entrepriseservice.affecterDepartementAEntreprise(departement.getId(), entreprise.getId());
		Assert.assertFalse(entrepriseservice.getAllDepartementsNamesByEntreprise(entreprise.getId()).isEmpty());
		LOG.info("affecterDepartementAEntreprise completed successfuly");
		LOG.info("End Method affecterDepartementAEntreprise");
	}

	public void getEntrepriseByIdTest() {
		LOG.info("-------------------- Start Method getEntrepriseById -------------------------");
		Entreprise entNames = entrepriseservice.getEntrepriseById(entreprise.getId());
		LOG.debug(entNames);
		LOG.info("entreprise has been geted successfly");
		LOG.info("--------------------- out getEntrepriseById --------------------------");
	}


	public void deleteEntrepriseByIdTest() {
		LOG.info("-------------------------- Start Method Delete Departement By Id ------------------------");
		LOG.info("The Dep will be deleted is : ", this.entreprise);
		entrepriseservice.ajouterEntreprise(this.entreprise);
		LOG.info(this.entreprise.getId());
		entrepriseservice.deleteEntrepriseById(this.entreprise.getId());
		LOG.info(this.entreprise.getId());
		Assert.assertFalse(entrepriserepo.findById(this.entreprise.getId()).isPresent());
		LOG.info(" Delete Entreprise By Id has been finished successfuly ");
		LOG.info("-------------------------- out of  deleteEntrepriseById ---------------------");


	}

}
